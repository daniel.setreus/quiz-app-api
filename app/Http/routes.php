<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/test', function () {
   echo "This is the route test";
});

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', ['middleware' => 'api.auth'], function ($api) {

	$api->get('info', function() {
		echo "This is the quiz API";
	});

	$api->post('customer', 'App\Http\Controllers\CustomersController@create')->name('CreateCustomer');
	
	$api->post('{customer}/project', 'App\Http\Controllers\ProjectsController@create');
	$api->get('{customer}/projects', 'App\Http\Controllers\ProjectsController@listProjects');

	$api->post('{customer}/{project}/quiz', 'App\Http\Controllers\QuizzesController@create');
	$api->get('{customer}/{project}/quizzes', 'App\Http\Controllers\QuizzesController@listQuizzes');

	$api->post('{customer}/{project}/{quiz}/question', 'App\Http\Controllers\QuestionsController@create');
	$api->get('{customer}/{project}/{quiz}/questions', 'App\Http\Controllers\QuestionsController@listQuestions');
	$api->get('{customer}/{project}/{quiz}/{question}', 'App\Http\Controllers\QuestionsController@viewQuestion');

	// Add choice to question
	// Link question to quiz

});
