<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\CreateQuizRequest;
use App\Project;
use App\Quiz;

class QuizzesController extends ApiBaseController
{
    
	public function create($customer, $project, CreateQuizRequest $request) {
		$project = Project::with('customer')
			->where('id', '=', $project)
			->orWhere('slug', '=', $project)->first();
		
		if(!$project) {
			return $this->response->errorBadRequest();
		}

		if(!$project->customer || !($project->customer->id == $customer || $project->customer->slug == $customer)) {
			return $this->response->errorBadRequest();
		}

		$quiz = new Quiz($request->all());
		$project->quizzes()->save($quiz);

		return response()->json($quiz->toArray());
	}

	public function listQuizzes($customer, $project) {

		$quiz = Quiz::whereHas('project', function($query) use($project) {
			if(is_numeric($project)) $key = 'id';
			else $key = 'slug';
			$query->where($key, '=', $project);
		})
		->whereHas('project.customer', function($query) use($customer) {
			if(is_numeric($customer)) $key = 'id';
			else $key = 'slug';
			$query->where($key, '=', $customer);
		})
		->get();

		return response()->json($quiz->toArray());
	}

}
