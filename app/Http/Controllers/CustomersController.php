<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCustomerRequest;
use App\Customer;

class CustomersController extends ApiBaseController
{

	public function create(CreateCustomerRequest $request) {
		try {
			$user = \Auth::user();
			$customer = new Customer($request->all());
			$user->customers()->save($customer);
		} catch(\Exception $e) {
			return $this->response->errorInternal();
		}
		return response()->json($customer->toArray());
	}

}
