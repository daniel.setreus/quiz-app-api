<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Http\JsonResponse;
use DB;

use App\Http\Requests\CreateQuestionsRequest;
use App\Quiz;
use App\Question;
use App\Choice;

class QuestionsController extends ApiBaseController
{
    
	public function create($customer, $project, $quiz, CreateQuestionsRequest $request) {
		$quizObj = $this->getQuizObject($customer, $project, $quiz);
		if(!$quizObj)
			return $this->response->errorNotFound('Endpoint error: Check the URL');
		
		DB::beginTransaction();
		try {
			$question = new Question($request->all());
			$question->save();
			$quizObj->questions()->attach(array($question->id));

			if(isset($request->choices)) {
				$choices = json_decode($request->choices, true);
				if(!is_array($choices)) 
					throw new \Exception("Choices is not an array", 1);

				foreach($choices as $choice) {
					if(!array_key_exists('title', $choice) || !array_key_exists('isCorrect', $choice))
						throw new \Exception("Atleast one choise is missing value for title or isCorrect", 1);
					if(!is_bool($choice['isCorrect']))
					 	throw new \Exception("Value for isCorrect must be of type boolean");
						
					$c = new Choice($choice);
					$question->choices()->save($c);
				}
			}
		} catch (\Exception $e) {
			DB::rollback();
			return new JsonResponse(
					array(
						'error' => 'Error saving question', 
						'message' => $e->getMessage(), 
						'request' => $request->all()
					), 400);
		}
		DB::commit();

		return response()->json($question->toArray());
	}

	public function listQuestions($customer, $project, $quiz) {
		$quizObj = $this->getQuizObject($customer, $project, $quiz);
		if(!$quizObj)
			return $this->response->errorNotFound('Endpoint error: Check the URL');

		return response()->json($quizObj->questions->toArray());
	}

	public function viewQuestion($customer, $project, $quiz, $question) {
		$question = Question::whereHas('quiz', function($query) use($quiz) {
			if(is_numeric($quiz)) $key = 'quizzes.id';
			else $key = 'slug';
			$query->where($key, '=', $quiz);
		})
		->whereHas('quiz.project', function($query) use($project) {
			if(is_numeric($project)) $key = 'projects.id';
			else $key = 'slug';
			$query->where($key, '=', $project);
		})
		->whereHas('quiz.project.customer', function($query) use($customer) {
			if(is_numeric($customer)) $key = 'customers.id';
			else $key = 'slug';
			$query->where($key, '=', $customer);
		})
		->with('choices')->find($question);

		if(!$question)
			return $this->response->errorNotFound('Endpoint error: Check the URL');	

		return response()->json($question->toArray());
	}

	private function getQuizObject($customer, $project, $quiz) {
		$quizObj = Quiz::with('project.customer', 'questions.choices')->where('id', '=', $quiz)->orWhere('slug', '=', $quiz)->first();
		if(!$quizObj)
			return false;

		// Test if project match
		if(!($quizObj->project->id == $project || $quizObj->project->slug == $project))
			return false;
		
		// Test if customer match
		if(!($quizObj->project->customer->id == $customer || $quizObj->project->customer->slug == $customer))
			return false;

		return $quizObj;
	}

}
