<?php
	namespace App\Http\Controllers;
	use Dingo\Api\Routing\Helpers;

	class ApiBaseController extends Controller {
		use Helpers;
	}
