<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProjectRequest;
use App\Customer;
use App\Project;


class ProjectsController extends ApiBaseController
{

	public function create($customer, CreateProjectRequest $request) {
		$customer = Customer::where('id', '=', $customer)->orWhere('slug', '=', $customer)->first();
		
		if(!$customer) {
			return $this->response->errorBadRequest();
		}
		$project = new Project($request->all());
		$customer->projects()->save($project);

		return response()->json($project->toArray());
	}

	public function listProjects($customer) {
		$c = Customer::with('projects')->where('id', '=', $customer)->orWhere('slug', '=', $customer)->first();
		if(!$c) {
			return $this->response->errorBadRequest();
		}

		return response()->json($c->projects->toArray());
	}
}
