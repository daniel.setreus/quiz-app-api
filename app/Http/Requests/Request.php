<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

abstract class Request extends FormRequest
{
	
	public function response(array $errors)
    {
        $rep = new JsonResponse(array('inputError' => 'Input data did not validate', 'errors' => $errors), 400);
        $rep->sendHeaders();
        $rep->sendContent();
        die();
        //return response()->json(array('validation_error' => true));
    }
}
