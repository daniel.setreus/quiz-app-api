<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    
    protected $fillable = array('title', 'score', 'order', 'supportiveText', 'allowInRandom', 'imageUrl');

	public function quiz() {
		return $this->belongsToMany('App\Quiz');
	}

	public function choices() {
		return $this->hasMany('App\Choice');
	}
}
