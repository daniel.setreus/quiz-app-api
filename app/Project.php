<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    
    protected $fillable = ['name'];

	public function customer() {
		return $this->belongsTo('App\Customer');
	}

	public function quizzes() {
		return $this->hasMany('App\Quiz');
	}

	public function setNameAttribute($value) {
		$this->attributes['name'] = ucfirst($value);
		if(!$this->slug)
			$this->attributes['slug'] = $this->makeSlugFromName($value);
	}

	public function makeSlugFromName($name) {
		$slug = str_slug($name);
		$count = Project::whereRaw("slug RLIKE '^{$slug}(-[0-9]+)?$'")->count();
		return $count ? "{$slug}-{$count}" : $slug;
	}

}
