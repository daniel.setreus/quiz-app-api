<?php
namespace App\Providers;

use Illuminate\Http\Request;
use Dingo\Api\Routing\Route;
use Dingo\Api\Contract\Auth\Provider;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use App\User;
use App\Customer;

class ApiAuthServiceProvider implements Provider
{
    public function authenticate(Request $request, Route $route)
    {
        // Logic to authenticate the request.
        //throw new UnauthorizedHttpException('Unable to authenticate with supplied API key');
        
        // var_dump($request->customer);
        // var_dump($request->route()->getName());
        // die;
    	
    	$user = $this->getUserFromApiKey($request);
    	if(!$user)
    		throw new UnauthorizedHttpException('Unable to authenticate with supplied API key');

    	\Auth::login($user);

    	if($request->route()->getName() !== 'CreateCustomer') {
			if(! $this->customerBelongsToUser($request, $user))
				throw new UnauthorizedHttpException('You have no access to the supplied customer');			
    	}

	}

    private function getUserFromApiKey(Request $request) {
    	if(!isset($request->apiKey))
    		return false;

    	return User::where('apiKey', '=', $request->apiKey)->first();
    }

    private function customerBelongsToUser(Request $request, User $user) {
    	if(!isset($request->customer))
    		return false;

    	$customer = Customer::where('id', '=', $request->customer)->orWhere('slug', '=', $request->customer)->first();
    	if(!$customer)
    		return false;

    	if($customer->user->id != $user->id)
    		return false;

    	return true;
    }
}