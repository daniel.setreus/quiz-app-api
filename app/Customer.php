<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{

	protected $fillable = ['name'];

	public function user() {
		return $this->belongsTo('App\User');
	}

	public function projects() {
		return $this->hasMany('App\Project');
	}

	public function setNameAttribute($value) {
		$this->attributes['name'] = ucfirst($value);
		if(!$this->slug)
			$this->attributes['slug'] = $this->makeSlugFromName($value);
	}

	public function makeSlugFromName($name) {
		$slug = str_slug($name);
		$count = Customer::whereRaw("slug RLIKE '^{$slug}(-[0-9]+)?$'")->count();
		return $count ? "{$slug}-{$count}" : $slug;
	}
}
