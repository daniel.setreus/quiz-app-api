<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{

	protected $fillable = ['name', 'passingScore', 'isRandom'];

    public function project() {
    	return $this->belongsTo('App\Project');
    }

    public function questions() {
    	return $this->belongsToMany('App\Question');
    }

    public function setNameAttribute($value) {
		$this->attributes['name'] = ucfirst($value);
		if(!$this->slug)
			$this->attributes['slug'] = $this->makeSlugFromName($value);
	}

	public function makeSlugFromName($name) {
		$slug = str_slug($name);
		$count = Quiz::whereRaw("slug RLIKE '^{$slug}(-[0-9]+)?$'")->count();
		return $count ? "{$slug}-{$count}" : $slug;
	}

}
