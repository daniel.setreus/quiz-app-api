# Quiz Backend API

The quiz backend API can be used to easily create and store quizzes, questions and individual results. A quiz belongs to a projects. A project in turn belongs to a customer. This creates custom API endpoints for your customers and their projects.

Built with [Laravel version 5.2](https://laravel.com) and the [Dingo API](https://github.com/dingo/api/) Extension

### Authors
* Daniel Setréus - [danielsetreus.se](http://danielsetreus.se)
* Simon Dahla

### Contribute / Bug reports
Please report bugs, or post suggestions, in the [repository issues tracking bord](https://gitlab.com/daniel.setreus/quiz-app-api/issues/). Please label issues appropriately. 

## Authentication
To use the API you must first obain a API key. __NOT IMPLEMENTED__. How should this be done? 

The API key must be included in *all* requests, and sent through the `apiKey` attribute. For example
> GET /api/example-inc/car-knowlede/quizzes?apiKey=123456789

would list all quizzes within the project Car Knowlede. 

Of course the API key user must have access to the customer (in the example above Example Inc).

Authentication errors will return a `status 401 Unauthorized` response.

__In the examples below the API key attribute have been omitted__ - but must be included in all request!

## A common note about requests
As you will see below request could contain as many as four query parameters (not counting the api key). The following objects can be identified using both their ID and their slug:

* Customers
* Projects
* Quizzes

In one request the use of ID or slug for identifying these objects can me mixed freely. A Question object must be identified with its ID - since it has no slug.

## Customers
A quiz must belong to a project - and a project must belong to a customer. A new customer can be created by calling the `/customer` endpoint. 

### POST /api/customer/
Creates a new customer.

Parameter  | Type | Description | Example
:-- | :-- | :-- | :--
name  | String | *Required* Custromer name | Example Customer

#### Returns
A successfull request retuns a `status 200` JSON response with the created customer object:

~~~ json
{
	"id": 8,
	"name": "Exampel Customer",
	"slug": "example-customer",
	"updated_at": "2016-05-06 08:44:30",
	"created_at": "2016-05-06 08:44:30",
}
~~~

Any error will return a none `status 200` response. Validation error (mission name paramter) returns `status 400 Bad Request` with validation errors: 

~~~ json
{
  "inputError": "Input data did not validate",
  "errors": {
    "name": [
      "The name field is required."
    ]
  }
}
~~~

## Projects
All projects belong to one customer. They are called on with the customer ID or slug in the URI.

### POST /api/{ customer-[ id | slug ] }/project
Creates a new project for a customer

Parameter  | Type | Description | Example
:-- | :-- | :-- | :--
name  | String | *Required* Project name | GIT training

__Example__
> * POST /api/example-customer/project/?name=GIT training
> * POST /api/8/project/?name=GIT Training

#### Returns
A successful request returns the created project object:

~~~ json
{
  "name": "GIT training",
  "slug": "git-training",
  "customer_id": 8,
  "updated_at": "2016-05-06 08:44:30",
  "created_at": "2016-05-06 08:44:30",
  "id": 4
}
~~~

### GET /api/{ customer-[ id | slug ] }/projects
Lists app projects for the given customer

__Example__
> GET /api/example-customer/projects
> 
> GET /api/8/projects

#### Returns
A `status 200` response containing a JSON array with project objects:

~~~ json
[
  {
    "id": 4,
    "name": "GIT training",
    "slug": "git-training",
    "customer_id": 8,
    "created_at": "2016-05-06 08:44:30",
    "updated_at": "2016-05-06 08:44:30"
  },
  ...
]
~~~

## Quizzes
This is the good stuff!

### POST /api/{ customer-[ id | slug ] }/{ project-[ id | slug ] }/quiz
Creates a new quiz for the given project.

__Example__
> POST /api/example-customer/git-training/quiz?title=name=Commands&passingScore=25

Parameter  | Type | Description | Example
:-- | :-- | :-- | :--
name  | String | *Required* Quiz name | Commands
passingScore | Integer | *Required* The total score required to pass the quiz | 25
isRandom | Boolean | If set to true this quiz will be auto populated with questions from the project. Defaults to *false*. __NOT IMPLEMANTED YET__ | false

#### Returns
A successful request will be given a `status 200` response with the created quiz object:

~~~ json
{
  "name": "Commands",
  "slug": "commands",
  "passingScore": "25",
  "project_id": 4,
  "updated_at": "2016-05-06 09:24:50",
  "created_at": "2016-05-06 09:24:50",
  "id": 4
}
~~~

### GET /api/{ customer-[ id | slug ] }/{ project-[ id | slug ] }/quizzes
Get all quizzes for a specified project (slug or id)

__Example__
> GET /api/metro-online/git-training/quizzes
> 
> GET /api/8/4/quizzes

#### Returns
A `status 200` respons with a JSON array of quiz objects:

~~~ json
[
  {
    "id": 4,
    "name": "Commands",
    "slug": "commands",
    "passingScore": 25,
    "isRandom": 0,
    "project_id": 4,
    "created_at": "2016-05-06 09:24:50",
    "updated_at": "2016-05-06 09:24:50"
  },
  ...
]
~~~

## Questions
A question can belong to multiple quizzes. This means that you can re-use questions inside a project for multiple quizzes. Perhaps you have need quizzes of different difficulties but with some common questions?

When creating a question it *must* be attatched to a quiz. However - you can later attatch that question to a different quiz as well.

### POST /api/{ customer-[ id | slug ] }/{ project-[ id | slug ] }/{ quiz-[id | slug ] }/question/
Creates a new question and attatches it to a quiz. 

__Example__
> POST /api/metro-online/git-training/commands/question?title=When would you use git clone?
> 
> POST /api/8/4/4/question/?title=When would you use git clone?

Parameter  | Type | Description | Default value | Example
:-- | :-- | :-- | :-- | :--
title  | String | *Required* The title of the question (the question itself?) | | When would you use git clone?
supportiveText | String | A supportative text to use with the question | null | In the terminal...
imageUrl | String | A URL for an image to use with the question | null |http://...
order | Integer | Ordering value for question (a low value will order it ahead of questions with a high value). | 1 | 1
score | Integer | The score to give if this question is answered correctly.| 1 | 1
allowInRandom | Boolean | If set to false this question can not be used when creating quizzes using random questions. | true | true
choices | JSON-String | If set the question is populated with choices. The format of this value is imprtant. See below.| null | [{"title": "Choice Title", "isCorrect": false}, ...]

#### The `choices` Paramater
If you want you can create the question, and attatch choices to it, in one request. To do so, add the `choices` parameter to the request. 

The value of `choices` must be a valid JSON array of choice objects. Example below:

~~~ json
[
	{
		"title": "When pushing", 
		"isCorrect": false
	},  
	{
		"title": "When cloning", 
		"isCorrect": true
	} 
]
~~~
If the value of `choices` is not valid JSON, or any of the choices inside `choices.choices` does not hold the required `title` and `isCorrect` attributes - and error `status 400 Bad Request` will be returned together with an informative JSON object.

#### Returns
If succesful the created question object will be returned:

~~~ json
{
  "title": "When would you use git clone?",
  "updated_at": "2016-05-06 11:15:10",
  "created_at": "2016-05-06 11:15:10",
  "id": 59
}
~~~

### GET /api/{ customer-[ id | slug ] }/{ project-[ id | slug ] }/{ quiz-[id | slug ] }/questions/

Returns all questions and their choices for the given quiz

__Example__
> GET /api/metro-online/git-training/commands/questions/
> 
> GET /api/8/4/4/questions/

#### Returns
An array with question objects, each holding the attribute `choices` - an array of `choice` objects. There is also the `pivot` object - showing the relationship between the question and the quiz:

~~~ json
[
	{
		"id": 54,
		"title": "When would you use git clone?",
		"score": 1,
		"order": 1,
		"supportiveText": null,
		"allowInRandom": 1,
		"imageUrl": null,
		"created_at": "2016-05-06 11:04:55",
		"updated_at": "2016-05-06 11:04:55",
		"pivot": {
			"quiz_id": 4,
			"question_id": 54
		},
		"choices": 
			[
				{
					"id": 17,
					"question_id": 54,
					"title": "When pushing",
					"isCorrect": 0,
					"created_at": "2016-05-06 11:04:55",
					"updated_at": "2016-05-06 11:04:55"
				},
				...
			]
		},
  ...
]
~~~ 

### GET /api/{ customer-[ id | slug ] }/{ project-[ id | slug ] }/{ quiz-[id | slug ] }/{ question-id }/
Returns the supplied question and its choices. Notice that the quetion does not have a slug, but must be identified by its ID. 

__Example__
> GET /api/example-inc/car-knowledge/vad-kan-du-om-volvo/60/
> 
> GET /api/11/5/6/60

#### Returns
On successful request the return will be a `status 200` containing the question object:

~~~ json
{
	"id": 60,
	"title": "Vilka färger finns Volvo i?",
	"score": 1,
	"order": 1,
	"supportiveText": null,
	"allowInRandom": 1,
	"imageUrl": null,
	"created_at": "2016-05-06 14:52:48",
	"updated_at": "2016-05-06 14:52:48",
	"choices": 
	[
		{
			"id": 23,
			"question_id": 60,
			"title": "Blue",
			"isCorrect": 0,
			"created_at": "2016-05-06 14:52:48",
			"updated_at": "2016-05-06 14:52:48"
		},
		...
	]
}
~~~