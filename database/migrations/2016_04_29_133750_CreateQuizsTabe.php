<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuizsTabe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quizzes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug')->unique();
            $table->integer('passingScore');
            $table->boolean('isRandom')->default(false);

            //$table->integer('customer_id')->unsigned();
            $table->integer('project_id')->unsigned();
            
            $table->timestamps();

            /*$table->foreign('customer_id')
                ->references('id')
                ->on('customers')
                ->onDelete('cascade');*/

            $table->foreign('project_id')
                ->references('id')
                ->on('projects')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('quizzes');
    }
}
